import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.io.*;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridLayout;

public class UserInterface{
  
  public File selectedFile;
  public int numOfVersions;
  
  public void runUserInterface() {
    
    JFrame frame = new JFrame("Locksmith");
    JMenuBar menuBar = new JMenuBar();
    
    JMenu menu = new JMenu("Locksmith");
    
    JMenuItem selectMenuItem = new JMenuItem("Select");
    selectMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent ev) {
        JFileChooser jfc = new JFileChooser();
        
        int returnValue = jfc.showOpenDialog(null);
        
        if(returnValue == JFileChooser.APPROVE_OPTION) {
          selectedFile = jfc.getSelectedFile();
          System.out.println(selectedFile.getAbsolutePath());
        }
      }
    });
    
    JMenuItem exitMenuItem = new JMenuItem("Exit");
    exitMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent ev) {
        System.exit(0);
      }
    });
    
    menu.add(selectMenuItem);
    menu.add(exitMenuItem);
    
    menuBar.add(menu);
    
    frame.setJMenuBar(menuBar);
    
    JPanel componentsPanel = new JPanel();
    componentsPanel.setLayout(null);
    
    JLabel titleLabel = new JLabel("Welcome to Locksmith!");
    JLabel versionsLabel = new JLabel("Enter the number of desired versions:");
    JTextField versionsText = new JTextField(10);
    versionsText.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent ev) {
        numOfVersions = Integer.parseInt(versionsText.getText());
      }
    });
    
    JButton runButton = new JButton("Run");
    runButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent ev) {
        FileParser fp = new FileParser();
        try {
          fp.parseFile(selectedFile, numOfVersions);
        }
        catch(Exception e) {
          System.exit(-1);
        }
      }
    });
    JLabel instructionLabel = new JLabel("Note: Remember to select a file to be shuffled");
    
    componentsPanel.add(titleLabel);
    componentsPanel.add(versionsLabel);
    componentsPanel.add(versionsText);
    componentsPanel.add(runButton);
    componentsPanel.add(instructionLabel);
    
    Insets insets = componentsPanel.getInsets();
    
    Dimension titleSize = titleLabel.getPreferredSize();
    titleLabel.setBounds(125 + insets.left, 25 + insets.top, titleSize.width, titleSize.height);
    
    Dimension versionsLabelSize = versionsLabel.getPreferredSize();
    versionsLabel.setBounds(85 + insets.left, 50 + insets.top, versionsLabelSize.width, versionsLabelSize.height);
    
    Dimension versionsTextSize = versionsText.getPreferredSize();
    versionsText.setBounds(130 + insets.left, 75 + insets.top, versionsTextSize.width, versionsTextSize.height);
    
    Dimension buttonSize = runButton.getPreferredSize();
    runButton.setBounds(155 + insets.left, 200 + insets.top, buttonSize.width, buttonSize.height);
    
    Dimension instructionSize = instructionLabel.getPreferredSize();
    instructionLabel.setBounds(55 + insets.left, 150 + insets.top, instructionSize.width, instructionSize.height);
    
    frame.add(componentsPanel);
      
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setPreferredSize(new Dimension(400,300));
    frame.pack();
    frame.setLocationRelativeTo(null);
    frame.setVisible(true);
  }
}