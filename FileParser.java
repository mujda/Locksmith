import java.io.*;
import java.util.*;

public class FileParser {

  public static void parseFile (File test, int numOfVersions) throws Exception{
    
    String filename = test.getName();
    FileReader fr = new FileReader(filename);
    BufferedReader br = new BufferedReader(fr);
    
    String line;
    String file = "";
    
    while((line = br.readLine()) != null) {
      file += line + "\n";
    }
    
    String[] questionsArray = file.split("\n\n", file.length());
    
    ArrayList<String> questionsList = new ArrayList<String>(Arrays.asList(questionsArray));
    
    //int numOfVersions = 3;
    
    for(int i=0; i < numOfVersions; i++) {
      Collections.shuffle(questionsList);
      
      String version_filename = "new_test" + i + ".txt";
      File f = new File(version_filename);
      FileOutputStream fout = new FileOutputStream(f);
      PrintStream out = new PrintStream(fout);
      
      for(int j=0; j < questionsList.size(); j++) {
        out.println(questionsList.get(j) + "\n");
      }
    }
    
    fr.close();
  }
} // parseFile

